Developer Guide
===============

This part of the SpeedCrunch manual encompasses further developer-focused resources.
It is targeted at people who wish to extend or contribute to SpeedCrunch.

.. toctree::
   :maxdepth: 2

   colorschemeformat
   docguide

.. TODO: Potential further topics:
..  - how to contribute
..  - code style guide
..  - ?
